#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Encounter bot

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, KeyboardButton, ReplyMarkup, bot, InlineKeyboardMarkup, InlineKeyboardButton, bot)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler, Handler, CallbackQueryHandler)

import logging
import gspread
from oauth2client.service_account import ServiceAccountCredentials

DEFAULT_GSHEET_URL = "https://docs.google.com/spreadsheets/d/1IG9aSYlopP5Ic0ihV7J9hEZdBKB05pn8S66gUEJb35I/edit#gid=0"

# вспомогательная функция поиска первой пустой клетки
def get_last(value_list):
    i = 0
    while value_list[i] != u'':
        i = i+1
    return (i)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# global
(DONE, LOGIN, ANS) = range(3)

class Message:
    def __init__(self, text, photo_url=None, timer=0):
        self.text = text
        self.photo_url = photo_url
        self.timer = timer

    def show(self, bot, chat_id, markup=None):
        bot.sendMessage(chat_id, text=self.text, reply_markup=markup)
        if self.photo_url is not None:
            bot.sendPhoto(chat_id, photo=self.photo_url)
        return

class Code:
    def __init__(self, ans):
        self.answer = ans
        self.done = False

    def is_done(self):
        return self.done

    def check_code(self, ans):
        # print(ans, self.answer)
        # print(ans == self.answer)
        if ans.lower() == self.answer.lower():
            self.done = True
        return self.done

    def get_ans(self):
        return self.answer

class Codes:
    def __init__(self, main_code, bonus_codes=None):
        self.main_code = main_code
        #bonus_codes code, first is bool true/false second is answer
        self.bonus_codes = bonus_codes

    def is_done(self):
        return self.main_code.is_done()

    def check_code(self, ans):
        out = self.main_code.check_code(ans)
        if self.bonus_codes is not None:
            for code in self.bonus_codes:
                code.check_code(ans)
        return out

    def show(self, bot, chat_id):
        if self.bonus_codes is not None:
            text = ['бонус{}: {}\n'.format(i, self.bonus_code.is_done() if self.bonus_code.get_ans() else "_________") for (i, bonus_code) in enumerate(self.bonus_codes)]
        else:
            text = []
        text = ['Главный код: {}'.format(self.main_code.is_done() if self.main_code.get_ans() else "_________")] + text
        bot.sendMessage(chat_id, text=text)
        return

class Question:
    def __init__(self, task_msg, codes, hint_msgs = None):
        self.task_msg = task_msg
        self.codes = codes
        self.hint_msgs = hint_msgs
        self.hint_num = 0
        self.done = False
        hint_button = InlineKeyboardButton(text="Подсказка", url=None, callback_data='hint')
        cancel_button = InlineKeyboardButton(text="Сдаюсь", url=None, callback_data='cancel')
        ans_keyboard = [[hint_button, cancel_button]]
        self.ans_markup =   InlineKeyboardMarkup(ans_keyboard)

    def show_task(self, bot, chat_id):
        self.task_msg.show(bot, chat_id, self.ans_markup)
        return

    def show_hint(self, bot, chat_id):
        if self.hint_num < len(self.hint_msgs):
            self.hint_msgs[self.hint_num].show(bot, chat_id, self.ans_markup)
            self.hint_num = self.hint_num+1
        else:
            bot.sendMessage(chat_id, text='Подсказок больше нет, лапоть')
        return

    def check_code(self, ans):
        self.done  = self.codes.check_code(ans)
        return self.done 

    def is_done(self):
        return self.done 

    def win(self, bot, chat_id):
        bot.sendMessage(chat_id, text='Правильно')
        bot.sendSticker(chat_id, sticker='BQADBQADEgADHAhTDDJYe4BK7AbEAg', quote=False)
        return

    def lose(self, bot, chat_id):
        bot.sendMessage(chat_id, text='Неправильно. Попроси подсказку как /hint')
        bot.sendSticker(chat_id, sticker='BQADAgADggMAAmvEygpB7-WnK73dGAI', quote=False)
        return

class Game:
    def __init__(self, gsheet_url=DEFAULT_GSHEET_URL, game_name="game3"):
        #авторизация и открытие гугл-таблицы
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name('ENbot-cred.json', scope)
        gc = gspread.authorize(credentials)
        worksheet = gc.open_by_url(gsheet_url).worksheet(game_name)
        self.cur_question_num = 0
        #получим число вопросов
        end_col = worksheet.find(u'!конец').col
        self.n_questions = int((end_col - 5)/2)
        #зададим вопросы
        self.questions = []
        for i in range(self.n_questions):
            #загадки
            #колонка вопросов
            q_col = 5+i*2
            q_values = worksheet.col_values(q_col)
            msg_text = q_values[1]
            msg_timer = q_values[2]
            task_msg = Message(msg_text, None, timer=msg_timer)
            #Подсказки
            #кол-во подсказок
            n_hints = int((get_last(q_values)-3)/2)
            hint_msgs = []
            for j in range(n_hints):
                h_msg = q_values[3+j*2]
                h_timer = q_values[3+j*2+1]
                hint_msgs.append(Message(h_msg, None, timer=h_timer))
            
            #отгадки
            #колонка ответов
            a_col = q_col+1
            a_values = worksheet.col_values(a_col)
            main_ans = a_values[1]
            main_code = Code(main_ans)
            #бонус-коды
            n_bonuses = int(get_last(a_values)-2)
            bonus_codes = []
            for j in range(n_bonuses):
                bonus_ans = a_values[2+j]
                bonus_codes.append(Code(bonus_ans))
            #все коды
            codes = Codes(main_code, bonus_codes)
            #добавить вопрос
            self.questions.append(Question(task_msg, codes, hint_msgs))

    def get_question(self, question_num):
        return self.questions[question_num]

    def get_cur_question(self):
        return self.questions[self.cur_question_num]

    def next_question(self):
        if self.cur_question_num == self.n_questions-1:
            return True
        else:
            self.cur_question_num = self.cur_question_num+1
            return False

class User:
    def __init__(self, chat_id, user_id, game_id, team_id):
        self.chat_id = chat_id
        self.user_id = user_id
        self.game_id = game_id
        self.team_id = team_id

    def get_game(self):
        return self.game_id
    def get_team(self):
        return self.team_id

def login_interface(bot, update):
    text = update.message.text
    num = int(text)
    user_id = num/100
    team_id = num%100
    game_id = team_id
    if game_id not in games:
        games[game_id] = Game()
    chat_id = update.message.chat_id
    user = User(chat_id, user_id, game_id, team_id)
    users.append(user)
    chats2users[chat_id] = user
    msg = Message('Игрок {}, Команда {}, Игра {}'.format(user_id, team_id, game_id))
    msg.show(bot, chat_id)
    chats.append(chat_id)
    return question_interface(bot, update)

def question_interface(bot, update):
    chat_id = update.message.chat_id
    user = chats2users[chat_id]
    game = games[user.get_game()]
    question = game.get_cur_question()
    question.show_task(bot, chat_id)
    return ANS

def answer_interface(bot, update):
    ans = update.message.text
    chat_id = update.message.chat_id
    user = chats2users[chat_id]
    game = games[user.get_game()]
    question = game.get_cur_question()
    got_it = question.check_code(ans)
    if(got_it == True):
        question.win(bot, chat_id)
        is_done = game.next_question()
        if(is_done==True):
            out = done_interface(bot, update)
        else:
            out = question_interface(bot, update)
    else:
        question.lose(bot, chat_id)
        out = ANS
    return out

def done_interface(bot, update):
    username = update.message.from_user.first_name
    msg = Message('Молодец, {}! Ты победил! /reset чтобы стереть себя и начать заново'.format(username), 'https://img-fotki.yandex.ru/get/9109/96658548.e/0_169f2b_39f750d6_orig.jpg')
    msg.show(bot, update.message.chat_id)
    return DONE

def hint_interface(bot, update):
    if(update.message is not None):
        message = update.message
    elif(update.callback_query.message is not None):
        message = update.callback_query.message
    chat_id = message.chat_id
    user = chats2users[chat_id]
    game = games[user.get_game()]
    question = game.get_cur_question()
    question.show_hint(bot, chat_id)
    return

def reset_interface(bot, update):
    chat_id = update.message.chat_id
    user = chats2users[chat_id]
    game_id = user.get_game()
    chats.remove(chat_id)
    users.remove(user)
    games[game_id] = []
    chats2users[chat_id] = []
    msg = Message('БАБАХ!!! /start для того, чтобы начать заново', 'https://xpda.com/junkmail/junk22/XX34.jpeg')
    msg.show(bot, chat_id)
    return ConversationHandler.END

def start(bot, update):

    username = update.message.from_user.first_name
    hello_msg = Message('Привет, {}'.format(username))
    hello_msg.show(bot, update.message.chat_id)

    if update.message.chat_id in chats:
        user = chats2users[update.message.chat_id]
        hello_msg = Message('Ты зарегистрирован в команде {}'.format(user.get_team()))
        hello_msg.show(bot, update.message.chat_id)
        out = question_interface(bot, update)
    else:
        hello_msg = Message('Ты еще не зарегистрирован, введи код:')
        hello_msg.show(bot, update.message.chat_id)
        out = LOGIN
    return out

def callback_interface(bot, update):
    data = update.callback_query.data
    if(data == "hint"):
        hint_interface(bot, update)
    elif(data == "cancel"):
        cancel(bot, update)

def cancel(bot, update):
    if(update.message is not None):
        message = update.message
    elif(update.callback_query.message is not None):
        message = update.callback_query.message
    user = message.from_user
    logger.info("User %s canceled the conversation." % user.first_name)
    message.reply_text('Лох, сдался')

    return ConversationHandler.END

def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))

users = []
chats = []
games = dict()
chats2users = dict()
def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token='308717900:AAFyncGrRQsnR5ZJ1SFRUAoioNRYAgNyUcI')

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={

            LOGIN: [MessageHandler(Filters.text, login_interface),
                    CommandHandler('cancel', cancel)],

            ANS: [MessageHandler(Filters.text, callback_interface),
                    CommandHandler('cancel', cancel), CommandHandler('hint', hint_interface)],

            DONE: [MessageHandler(Filters.text, done_interface),
                    CommandHandler('reset', reset_interface)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)
    dp.add_handler(CallbackQueryHandler(callback_interface))

    # log all errors
    dp.add_error_handler(error)
    # Start the Bot
    updater.start_polling()

    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
