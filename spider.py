#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.support.ui import Select

# -------------------------------------------------------------------------------------------------
# CONSTANTS ---------------------------------------------------------------------------------------

FIRST_NAME = 'Anton'
LAST_NAME = 'Afanasev'
APPLICANT_EMAIL = 'anton.afanasev@phystech.edu'
APPLICANT_PRIMARY_CONTACT_NUMBER = '8914818411'
PASSPORT_NUMBER = '999999999'
DATE_OF_BIRTH = '08 Feb 1994'
GENDER = 'Male'
DATE_OF_INTENDED_TRAVEL = '20 Apr 2017'
LOCATION = 'RUSSIA'
COUNTRY_OF_NATIONALITY = 'RUSSIA'
#------------------------------------------------------------------------------------------
# hardcoded constants ---------------------------------------------------------------------
REASON_FOR_VISIT = 'Study'
VISA_TYPE = 'Study (non Points-Based System)'
VISA_SUB_TYPE = 'Study - Short-term student 6 months'
IF_SETTLED_RELATIVE = 'no'
MORE_THAN_11_MONTHS = 'no'
CAS_REFERENCE_NUMBER = 'no'
#------------------------------------------------------------------------------------------
CHANGED_NAME = 'No'
PLACE_OF_BIRTH = 'Miass'
CHANGED_NATIONALITY = 'No'
PLACE_OF_ISSUE = 'Dolgoprudny'
ISSUING_AUTHORITY = 'MFC'
DATE_OF_ISSUE = '21 Mar 2014'
DATE_OF_EXPIRY = '21 Mar 2024'
IS_CITIZEN_IN_APPLYING_COUNTRY = 'Yes'
IS_FIRST_PASSPORT = 'Yes'
IS_TRAVELLING_WITH_ANYONE = 'No'
STAYING_TIME = '30'
STAYING_ADDRESS = 'Mount Pleasant Mail Centre'
STAYING_CONTACT_PRIM = '23232323'
RESIDENTIAL_ADDRESS = 'Miass, Molodezhnaya 8'
COUNTRY_OF_RESIDENCE = 'RUSSIA'
TIME_IN_CURRENT_RESIDENCE = '18 years'
IS_PREFERRED_DIFFERS_FROM_PERM = 'No'
ISSUED_VISAS_TO_UK_OR_COMM = 'No'
TRAVELLED_VISAS_TO_UK_OR_COMM = 'No'
MADE_APPLICATIONS_TO_HOME_OFFICE = 'No'
REFUSED_ENTRY_TO_UK = 'No'
REFUSED_VISA_TO_ANY_COUNTRY = 'No'
DEPORTED_FROM_ANY_COUNTRY = 'No'
VOLUNTARILY_ELECTED_TO_LEAVE = 'No'
IS_SUBJECT_TO_EXCLUSION_ORDER = 'No'
TRAVELLED_OUTSIDE_COUNTRY_OF_RESIDENCE = 'No'
ISSUED_WITH_INSUARANCE_NUMBER = 'No'
CONVICTED_OF_ANY_OFFENCE = 'No'
ARRESTED_AND_CHARGED = 'No'
SUPPORTED_TERRORIST_ACTIVITIES = 'No'
JUSTIFIED_TERRORIST_VIOLENCE = 'No'
INVOLVED_IN_WAR_CRIMES = 'No'
INDICATED_NOT_TO_BE_ADMITTED_TO_UK = 'No'
INTERVIEW_LANGUAGE = 'English'
ARE_RECIEVED_PENALTIES = 'No'
IS_JUDGEMENT_FOR_DEBTS = 'No'

# ------------------------------------------------------------------------------------------

def fill_space(driver, id, text):
    elem = driver.find_element_by_id(id)
    elem.send_keys(text)
    
def fill_form(driver, id, text):
    select = Select(driver.find_element_by_id(id))
    select.select_by_visible_text(text)
    
def click_rad_class(driver, name, class_name):
    driver.find_element_by_xpath("//input[@class='{}'][@name='{}']".format(class_name, name)).click()
    
def click_rad_value(driver, name, value):
    driver.find_element_by_xpath("//input[@value='{}'][@name='{}']".format(value, name)).click()

def get_spider():
	driver = webdriver.Chrome()
	driver.implicitly_wait(10)
	driver.get("https://www.visa4uk.fco.gov.uk/home/welcome")
	driver.find_element_by_xpath('//a[text()="Log in"]').click()

	with open('user.txt', 'r') as f:
		read_data = f.read()
	EMAIL, PASSW = read_data.split()

	fill_space(driver, 'Email', EMAIL)
	fill_space(driver, 'Password', PASSW)
	driver.find_element_by_id('btnSubmit').click()

	driver.find_element_by_xpath('//a[text()="Apply For Someone Else"]').click()

	driver.find_element_by_xpath("//input[@type='button'][@value='Continue']").click()

	fill_space(driver, 'FirstName', FIRST_NAME)
	fill_space(driver, 'LastName', LAST_NAME)
	fill_space(driver, 'ApplicantEmail', APPLICANT_EMAIL)
	fill_space(driver, 'ApplicantPrimaryContactNumber', APPLICANT_PRIMARY_CONTACT_NUMBER)
	fill_space(driver, 'PassportNumber', PASSPORT_NUMBER)
	fill_space(driver, 'datePicker', DATE_OF_BIRTH)

	fill_form(driver, 'GenderType', GENDER)

	fill_space(driver, 'ditDatePicker', DATE_OF_INTENDED_TRAVEL)

	fill_form(driver, 'Location', LOCATION)
	fill_form(driver, 'Nationality', COUNTRY_OF_NATIONALITY)

	fill_form(driver, 'Reason', REASON_FOR_VISIT)
	fill_form(driver, 'VisaType', VISA_TYPE)
	fill_form(driver, 'VisaSubType', VISA_SUB_TYPE)

	click_rad_class(driver, 'doc0', IF_SETTLED_RELATIVE)
	click_rad_class(driver, 'doc1', MORE_THAN_11_MONTHS)
	click_rad_class(driver, 'doc2', CAS_REFERENCE_NUMBER)

	driver.find_element_by_id('btnSubmit').click()

	# first button
	driver.find_element_by_xpath("//a[i]").click()

	# �������� ��� �������� �������
	click_rad_value(driver, 'rad30', CHANGED_NAME)

	fill_space(driver, 'txt60', PLACE_OF_BIRTH)

	click_rad_value(driver, 'rad90', CHANGED_NATIONALITY)

	fill_space(driver, 'txt120', PLACE_OF_ISSUE)
	fill_space(driver, 'txt130', ISSUING_AUTHORITY)
	fill_space(driver, 'cal140', DATE_OF_ISSUE)
	fill_space(driver, 'cal150', DATE_OF_EXPIRY)

	click_rad_value(driver, 'rad170', IS_CITIZEN_IN_APPLYING_COUNTRY)
	click_rad_value(driver, 'rad190', IS_FIRST_PASSPORT)
	click_rad_value(driver, 'rad210', IS_TRAVELLING_WITH_ANYONE)

	fill_space(driver, 'txt240', STAYING_TIME)
	fill_space(driver, 'addadd1250', STAYING_ADDRESS)
	fill_space(driver, 'addland250', STAYING_CONTACT_PRIM)

	driver.find_element_by_id('btnNext').click()

	fill_space(driver, 'addadd1260', RESIDENTIAL_ADDRESS)
	fill_form(driver, 'addctry260', COUNTRY_OF_RESIDENCE)
	fill_space(driver, 'addland260', APPLICANT_PRIMARY_CONTACT_NUMBER)
	fill_space(driver, 'txt270', TIME_IN_CURRENT_RESIDENCE)

	click_rad_value(driver, 'rad280', IS_PREFERRED_DIFFERS_FROM_PERM)
	click_rad_value(driver, 'rad300', ISSUED_VISAS_TO_UK_OR_COMM)
	click_rad_value(driver, 'rad320', TRAVELLED_VISAS_TO_UK_OR_COMM)
	click_rad_value(driver, 'rad340', MADE_APPLICATIONS_TO_HOME_OFFICE)
	click_rad_value(driver, 'rad360', REFUSED_ENTRY_TO_UK)
	click_rad_value(driver, 'rad380', REFUSED_VISA_TO_ANY_COUNTRY)
	click_rad_value(driver, 'rad400', DEPORTED_FROM_ANY_COUNTRY)
	click_rad_value(driver, 'rad420', VOLUNTARILY_ELECTED_TO_LEAVE)
	click_rad_value(driver, 'rad440', IS_SUBJECT_TO_EXCLUSION_ORDER)
	click_rad_value(driver, 'rad460', TRAVELLED_OUTSIDE_COUNTRY_OF_RESIDENCE)
	click_rad_value(driver, 'rad480', ISSUED_WITH_INSUARANCE_NUMBER)
	click_rad_value(driver, 'rad510', CONVICTED_OF_ANY_OFFENCE)
	click_rad_value(driver, 'rad530', ARRESTED_AND_CHARGED)
	click_rad_value(driver, 'rad550', SUPPORTED_TERRORIST_ACTIVITIES)
	click_rad_value(driver, 'rad570', JUSTIFIED_TERRORIST_VIOLENCE)
	click_rad_value(driver, 'rad590', INVOLVED_IN_WAR_CRIMES)
	click_rad_value(driver, 'rad610', INDICATED_NOT_TO_BE_ADMITTED_TO_UK)

	fill_space(driver, 'txt630', INTERVIEW_LANGUAGE)

	click_rad_value(driver, 'rad640', ARE_RECIEVED_PENALTIES)
	click_rad_value(driver, 'rad660', IS_JUDGEMENT_FOR_DEBTS)

	driver.find_element_by_id('btnNext').click()