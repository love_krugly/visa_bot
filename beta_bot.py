#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, bot, InlineKeyboardMarkup, InlineKeyboardButton, KeyboardButton)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler, CallbackQueryHandler, Handler)
import numpy as np
import gspread
from oauth2client.service_account import ServiceAccountCredentials

import logging
import nltk
import nltk.corpus
import string
from nltk.stem import SnowballStemmer
from spider import get_spider as gs
import urllib3
urllib3.disable_warnings()

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

def get_last(value_list):
    i = 0
    while value_list[i] != u'':
        i = i+1
    return (i)

class QuestionsAccumulator:
    def __init__(self):
        self.synonyms = []
        self.tokens = []  
        self.index2id = []
        self.stemmer = SnowballStemmer("russian")
        self.update_from_spreadsheet()
        self.questions2tokens()

    def questions2tokens(self):
        self.stopwords = nltk.corpus.stopwords.words('russian')
        self.stopwords.extend(string.punctuation)
        self.stopwords.append('')
        for a in self.synonyms:
            self.tokens.append([self.stemmer.stem(token.lower().strip(string.punctuation)) for token in nltk.word_tokenize(a) \
                if self.stemmer.stem(token.lower().strip(string.punctuation)) not in self.stopwords])

    def update_from_spreadsheet(self):
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name('Applications Spreadsheet-b4dedf3466a6.json', scope)
        gc = gspread.authorize(credentials)
        sh = gc.open_by_key('1taSxQhSOLFoGZtl-L913EuFiKYpMjLLLbkL3k7ow2WY')
        self.wks = sh.worksheet("synonyms")
        q_dict = dict()
        end_ind=get_last(self.wks.col_values(1))
        ids, qs = self.wks.col_values(1)[1:end_ind], self.wks.col_values(2)[1:end_ind]
        ids = list(map(int, ids))
        self.index2id = ids
        self.synonyms = qs

    def add__to_spreadsheet(self, idd, q):
        # для всех не добавленных ID:Q пар из new_idq добавляем строку
        self.wks.update_cell(len(self.index2id)+2, 1, idd)
        self.wks.update_cell(len(self.index2id)+2, 2, q)
        self.index2id.append(idd)
        self.tokens.append([self.stemmer.stem(token.lower().strip(string.punctuation)) for token in nltk.word_tokenize(q) \
            if self.stemmer.stem(token.lower().strip(string.punctuation)) not in self.stopwords])

class Global:
    def __init__(self):
        self.questions = []
        self.update_from_spreadsheet()
        self.users = []
        self.chats = []
        self.chats2users = dict()

    def update_from_spreadsheet(self):
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name('Applications Spreadsheet-b4dedf3466a6.json', scope)
        gc = gspread.authorize(credentials)
        sh = gc.open_by_key('1taSxQhSOLFoGZtl-L913EuFiKYpMjLLLbkL3k7ow2WY')
        wks = sh.worksheet("visa_application")
        end_ind=get_last(wks.col_values(1))
        q_dict = dict()
        ids, qs_next, qs, answs = wks.col_values(1)[1:end_ind], wks.col_values(2)[1:end_ind], wks.col_values(3)[1:end_ind], wks.col_values(4)[1:end_ind]
        print(ids)
        print(qs_next)
        print(qs)
        print(answs)
        ids = list(map(int, ids))
        for i in range(len(ids)):
            new_q_next = list(map(int, qs_next[i].split()))
            new_q = Question(ids[i], new_q_next, qs[i], answs[i])
            q_dict[ids[i]] = new_q

        self.questions = q_dict

class User:
    def __init__(self, chat_id):
        self.chat_id = chat_id
        self.question_id = 0
        self.unmatched = []
    # Обработка интерфейса
    def ask_to_add(self, message, quest_id):
        if self.unmatched:
            message.reply_text(u"На какие из предыдущих вопросов подходит этот ответ?")
            for text in self.unmatched:
                keyboard = []
                keyboard = [[InlineKeyboardButton(text="Да", callback_data='yes'+str(quest_id)), InlineKeyboardButton(text="Нет", callback_data='no')]]
                ans_markup = InlineKeyboardMarkup(keyboard)
                message.reply_text(text, reply_markup=ans_markup)

    def add(self, message, response):
        if(response[:3] == 'yes'):
            quest_id = int(response[3:])
            # message.reply_text("Да. Спасибо!")
            if self.unmatched:
                for text in self.unmatched:
                    the_sphinx.add__to_spreadsheet(quest_id, text)

        elif(response[:2] == 'no'):
            self.unmatched = []
            # message.reply_text("Нет. Спасибо!")
        self.unmatched = []

class Question:
    def __init__(self, id, q_next, question, answer):
        self.id = id
        self.q_next = q_next
        self.question = question
        self.answer = answer

        #При первом /start добавляем юзера в словарь известных the_god.users
#Связываем User с его chat_id, чтобы потом, прочитав chat_id из update.message, его идентифицировать
def login_interface(bot, update):
    chat_id = update.message.chat_id
    user = User(chat_id)
    if(user not in the_god.users):
        the_god.users.append(user)
        the_god.chats.append(chat_id)
        the_god.chats2users[chat_id] = user
    
    keyboard = []
    for q in the_god.questions[0].q_next:
        keyboard.append([InlineKeyboardButton(text=the_god.questions[q].question, callback_data=str(q))])
    ans_markup = InlineKeyboardMarkup(keyboard)
    KB_FORWBACK = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(u'⬅ Назад',), KeyboardButton(u'Далее ➡',)],],resize_keyboard=True,)
    bot.sendMessage(chat_id, text=u"Отлично, приступим к работе! ", reply_markup=KB_FORWBACK)
    if(the_god.questions[0].answer):
    	update.message.reply_text(the_god.questions[0].answer, reply_markup=ans_markup)
    return ANS

def callback_interface(bot, update):
	text = " "
	DO_ASK = False
	if (update.callback_query is not None):
		message = update.callback_query.message
		user = the_god.chats2users[message.chat_id] 
		data = update.callback_query.data
		if (data == 'start'):
			data = '1'
		elif (data[:3] == 'yes' or data == 'no'):
			user.add(message, data)
			text = u'Спасибо'
		else:
			user.question_id = int(data)
			user.unmatched = []
			if not the_god.questions[user.question_id].answer:
				text = u'Нет текста вопроса № {}'.format(user.question_id)
			else:
				text = the_god.questions[user.question_id].answer
#Интерфейс вопроса+ответа. В идеале определяет нужный вопрос хитро, функцией get_id_from_message
#Справшивает вопрос по его id
	elif(update.message is not None):
		message = update.message
		user = the_god.chats2users[message.chat_id]
# Обработка кнопок вперед/назад
		if message.text != u'Далее ➡' and message.text != u'⬅ Назад':
			user.question_id, prob = get_id_from_message(update.message)
			tresh = 0.3
			if (prob < tresh):
				user.unmatched.append(message.text)
				text = "Попробуй спросить по-другому..."
			else:
				DO_ASK = True
				if not the_god.questions[user.question_id].answer:
					text = u'Нет текста вопроса № {}'.format(user.question_id)
				else:
					text = the_god.questions[user.question_id].answer
		else:
			text = prev_next(message)  


	if the_god.questions[user.question_id].id == 127:
		#gs()
		message.reply_text('Анкета заполнилась')
	else:
		keyboard = []
		for q in the_god.questions[user.question_id].q_next:
			keyboard.append([InlineKeyboardButton(text=the_god.questions[q].question, callback_data=str(q))])
		ans_markup = InlineKeyboardMarkup(keyboard)
    #     bot.sendMessage(update.callback_query.message.chat_id, text=the_god.questions[quest_id].question)

		message.reply_text(text, reply_markup=ans_markup)
		if DO_ASK == True:
			user.ask_to_add(message, user.question_id)
	return ANS

# Обработка кнопок вперед/назад
def prev_next(message):
    user = the_god.chats2users[message.chat_id] 
    if message.text == u'Далее ➡':
        if user.question_id >= len(the_god.questions):
            user.question_id = len(the_god.questions)-1
        else:
            user.question_id += 2
        text = u'Следующие вопросы:'
    elif message.text == u'⬅ Назад':
        if user.question_id <= 0:
            user.question_id = 0
        else:
            user.question_id -= 2
        text = u'Предыдущие вопросы:'
    user.unmatched = []
    return text
#Машин лернинг тут
def get_id_from_message(message):
    tresh = 0.7
    text = message.text
# берем основу от только значащих слов
    token_in = [the_sphinx.stemmer.stem(token.lower().strip(string.punctuation)) for token in nltk.word_tokenize(text) \
                        if the_sphinx.stemmer.stem(token.lower().strip(string.punctuation)) not in the_sphinx.stopwords]
# Calculate Jaccard similarity
    # print(the_sphinx.stemmer.stem(token.lower().strip(string.punctuation))) 
    probs = [len(set(token_in).intersection(tokens_b)) / (1e-10 + float(len(set(token_in).union(tokens_b)))) for tokens_b in the_sphinx.tokens]
    ind = np.argsort(probs)

    return the_sphinx.index2id[ind[-1]], probs[ind[-1]]

#Если бот надоел его можно послать нахуй
def cancel(bot, update):
    update.message.reply_text(u'Рестарт')
    return ConversationHandler.END

#Станет понятнее
def help(bot, update):
    update.message.reply_text(u'\tHelp:\n 🌚 Прежде всего, не забывайте писать боту любые вопросы - он умеет на них отвечать и постоянно совершенствуется!\n 🌚 /start в начале \n 🌚 /cancel для ресета \n 🌚 Кнопки Назад ⬅/⬅ Дальше для перемещения по анкете')
    return ConversationHandler.END

#Когда все завершено
def done_interface(bot, update):
    username = update.message.from_user.first_name
    text = u'Пока'
    bot.sendMessage(chat_id, text=text)
    return ConversationHandler.END

# Это для logger'a
def error(bot, update, error):
    logger.warn(u'Update "%s" caused error "%s"' % (update, error))

#Все, что есть глобальное тут:
the_god = Global()
the_sphinx = QuestionsAccumulator()

(DONE, ANS) = range(2)

def main():
    updater = Updater(token='294309492:AAHLCtGCqqECKq_GtbMv-xyVlc3b1rpsMyM')
    # updater = Updater(token='357629118:AAHRFnsiexrlFWyzJg6C5m1KkDCyNNB55yc')

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', login_interface)],

        states={

            ANS: [MessageHandler(Filters.text, callback_interface), CommandHandler('help', help),
                    CommandHandler('cancel', cancel), CommandHandler('ask', callback_interface)],

            DONE: [MessageHandler(Filters.text, done_interface),
                    CommandHandler('reset', cancel)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)
    dp.add_handler(CallbackQueryHandler(callback_interface))

    # Start the Bot
    updater.start_polling()
        # log all errors
    dp.add_error_handler(error)
    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':

    main()